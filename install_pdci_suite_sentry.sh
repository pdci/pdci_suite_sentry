#!/usr/bin/env bash
rm -rf ./.env
set -e

usage()
{
    echo "======================================================================================="
    echo "Selecione em qual ambiente ira instalar:                           "
    echo ""
    echo "      -tcti | tcti               Ambiente de Teste Colaborativo da Tecnologia da Informacao"
    echo "      -prd  | prd                Ambiente de Produção (Usúario Final)"
    echo "======================================================================================="
}

#export_env()
#{
#rm -rf export.env
#cat .env | while read line
#do
#    echo "export $line" >> export.env
#done
#export
#echo ""
#echo ""
#cat export.env
#echo ""
#echo ""
#source export.env
#echo ""
#echo ""
#}

#pdci_docker_host_dev=tcp://192.168.56.133:2375
#pdci_docker_host_tcti=tcp://10.197.93.75:2375
#pdci_docker_host_hmg=tcp://10.197.94.75:2375
#pdci_docker_host_prd=tcp://10.197.32.76:2375

case $1 in
    -tcti| tcti )
     #  cp env.tcti .env
     #  export_env
       __pdci_docker_host=tcp://10.197.93.75:2375
       __pdci_application_env=tcti
    ;;
    -prd | prd )
      # cp env.prd .env
      # export_env
       __pdci_docker_host=tcp://10.197.93.75:2375
       __pdci_application_env=production
    ;;
    -h | --help )
        usage
        exit
    ;;
    * )
        usage
        exit 1
esac

if [ "${__pdci_docker_host}" == "" ]; then

    echo "ERRO :: Ambiente nao foi informado"
    usage
    exit 1
fi

verifica_instalacao_docker()
{
existe=$(which docker)

if [ "${existe}" == "" ]; then
    echo "ERRO :: DOCKER NAO ESTA INSTALADO"
    exit 1
fi

existe=$(which docker-compose)
if [ "${existe}" == "" ]; then
    echo "ERRO :: DOCKER COMPOSER NAO ESTA INSTALADO"
    exit 1
fi

}

#verifica_instalacao_docker

# TODO FAZER COM QUE O SCRIPT DESCUBRA QUAIS SAO OS SERVICOS QUE VEEM DO REGISTRY E APENAS NESTES REALIZE O PULL
docker-compose  -H ${__pdci_docker_host}  -p pdci_suite_sentry_${__pdci_application_env} -f  docker-compose.yml  pull
echo ""
echo "Removendo containers do pdci suite ..."
echo ""
docker-compose  -H ${__pdci_docker_host} -p pdci_suite_sentry_${____pdci_application_env} -f  docker-compose.yml rm -f -s

docker  -H ${__pdci_docker_host}  volume prune -f
docker  -H ${__pdci_docker_host}  container prune -f

echo "docker-compose  -H ${__pdci_docker_host}    -p pdci_suite_sentry_${__pdci_application_env} -f docker-compose.yml config"
docker-compose  -H ${__pdci_docker_host}  -p pdci_suite_sentry_${__pdci_application_env} -f docker-compose.yml config
echo ""
echo ""
docker-compose  -H ${__pdci_docker_host}  -p pdci_suite_sentry_${__pdci_application_env} -f docker-compose.yml  up -d
#rm -f export.env
echo ""
echo ""
#cat .env
#rm -f .env
echo ""
echo ""
docker-compose  -H ${__pdci_docker_host}  -p pdci_suite_sentry_${__pdci_application_env} -f docker-compose.yml  ps

docker-compose  -H ${__pdci_docker_host}  -p pdci_suite_sentry_${__pdci_application_env} -f docker-compose.yml  logs -f