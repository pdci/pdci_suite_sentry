#!/usr/bin/env bash
rm -rf ./.env
set -e

usage()
{
    echo "======================================================================================="
    echo "Selecione em qual ambiente ira instalar:                           "
    echo ""
    echo "      -tcti | tcti               Ambiente de Teste Colaborativo da Tecnologia da Informacao"
    echo "      -hmg  | hmg                Ambiente de Homologacao "
    echo "      -prd  | prd                Ambiente de Produção (Usúario Final)"
    echo "======================================================================================="
}

#export_env()
#{
#rm -rf export.env
#cat .env | while read line
#do
#    echo "export $line" >> export.env
#done
#export
#echo ""
#echo ""
#cat export.env
#echo ""
#echo ""
#source export.env
#echo ""
#echo ""
#}

#pdci_docker_host_dev=tcp://192.168.56.133:2375
#pdci_docker_host_tcti=tcp://10.197.93.75:2375
#pdci_docker_host_hmg=tcp://10.197.94.75:2375
#pdci_docker_host_prd=tcp://10.197.32.76:2375

case $1 in
    -tcti| tcti )
     export PDCI_FILE_ENV_DOCKER=./.env.docker.tcti

       __pdci_docker_host=tcp://10.197.93.75:2375
       __pdci_application_env=tcti
       export PDCI_APPLICATION_ENV=${__pdci_application_env}
    ;;
    -prd | prd )
      export PDCI_FILE_ENV_DOCKER=./.env.docker.prd
       __pdci_docker_host=tcp://10.197.32.76:2375
       __pdci_application_env=production
       export PDCI_APPLICATION_ENV=production
    ;;
    -hmg | hmg )
      export PDCI_FILE_ENV_DOCKER=./.env.docker.hmg
       __pdci_docker_host=tcp://10.197.94.75:2375
       __pdci_application_env=homologacao
       export PDCI_APPLICATION_ENV=${__pdci_application_env}
    ;;
    -h | --help )
        usage
        exit
    ;;
    * )
        usage
        exit 1
esac

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | xargs)



if [ "${__pdci_docker_host}" == "" ]; then

    echo "ERRO :: Ambiente nao foi informado"
    usage
    exit 1
fi

verifica_instalacao_docker()
{
existe=$(which docker)

if [ "${existe}" == "" ]; then
    echo "ERRO :: DOCKER NAO ESTA INSTALADO"
    exit 1
fi

existe=$(which docker-compose)
if [ "${existe}" == "" ]; then
    echo "ERRO :: DOCKER COMPOSER NAO ESTA INSTALADO"
    exit 1
fi

}

#SENTRY_SERVER_EMAIL
#The email address used for From: in outbound emails. Default: root@localhost
#
#SENTRY_EMAIL_HOST, SENTRY_EMAIL_PORT, SENTRY_EMAIL_USER, SENTRY_EMAIL_PASSWORD, SENTRY_EMAIL_USE_TLS
#Connection information for an outbound smtp server. These values aren't needed if a linked smtp container exists.

#docker -H ${__pdci_docker_host} volume create data_sentry_pgdata
#docker -H ${__pdci_docker_host} volume create data_sentry_conf
#docker -H ${__pdci_docker_host} volume create data_sentry_files
#
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped --name sentry-redis redis
#
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped -v data_sentry_conf:/var/lib/postgresql/data  --name sentry-postgres -e POSTGRES_PASSWORD=secret -e POSTGRES_USER=sentry postgres
#
#
#docker -H ${__pdci_docker_host} run -it --rm -m 2048m --cpu-shares=512 --name sentry-upgrade -e SENTRY_SECRET_KEY='d862-e89gf!jf49^fj410ps#z5%afei=2=j#z4pr(g1x^frdbr' --link sentry-postgres:postgres --link sentry-redis:redis sentry upgrade
#
#
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped --name my-sentry -v data_sentry_files:/var/lib/sentry/files -e SENTRY_SERVER_EMAIL=redmine-projetos@icmbio.gov.br -e SENTRY_EMAIL_HOST=mail.icmbio.gov.br -p 9009:9000 -e SENTRY_SECRET_KEY='d862-e89gf!jf49^fj410ps#z5%afei=2=j#z4pr(g1x^frdbr' --link sentry-redis:redis --link sentry-postgres:postgres sentry
#
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped --name sentry-cron -e SENTRY_SECRET_KEY='d862-e89gf!jf49^fj410ps#z5%afei=2=j#z4pr(g1x^frdbr' --link sentry-postgres:postgres --link sentry-redis:redis sentry run cron
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped --name sentry-worker-1 -e SENTRY_SECRET_KEY='d862-e89gf!jf49^fj410ps#z5%afei=2=j#z4pr(g1x^frdbr' --link sentry-postgres:postgres --link sentry-redis:redis sentry run worker


#Instalacao com docker composer

# TODO FAZER COM QUE O SCRIPT DESCUBRA QUAIS SAO OS SERVICOS QUE VEEM DO REGISTRY E APENAS NESTES REALIZE O PULL
docker-compose  -H ${__pdci_docker_host}  -p pdci_${__pdci_application_env} -f  docker-compose.yml  pull
echo ""
echo "Removendo containers do pdci suite ..."
echo ""
docker-compose  -H ${__pdci_docker_host} -p pdci_${__pdci_application_env} -f  docker-compose.yml rm -f -s

echo "docker-compose  -H ${__pdci_docker_host} -p pdci_${__pdci_application_env} -f docker-compose.yml config"
docker-compose  -H ${__pdci_docker_host}  -p pdci_${__pdci_application_env} -f docker-compose.yml config
echo ""
echo ""
docker-compose  -H ${__pdci_docker_host}  -p pdci_${__pdci_application_env} up -d
echo ""
docker-compose  -H ${__pdci_docker_host}  -p pdci_${__pdci_application_env} -f docker-compose.yml  ps
echo ""

echo "******** caso seja a instalacao inicial sera necessario realizar as sequintes ações ******** "
echo ""
export PDCI_FILE_ENV_DOCKER=./.env.docker.prd
 export PDCI_APPLICATION_ENV=production

echo "docker-compose  -H ${__pdci_docker_host}  -p pdci_${__pdci_application_env} exec app_sentry sentry upgrade"
echo "docker-compose  -H ${__pdci_docker_host}  -p pdci_${__pdci_application_env} restart app_sentry "

echo ""
echo ""
sleep 5
verificar_status_container () {
docker-compose  -H ${__pdci_docker_host} -p pdci_${__pdci_application_env} -f docker-compose.yml ps | awk -F " " ' { print $1 }' | sed "1d" | sed "1d" > lista_servicos
cat lista_servicos | while read line
do
	echo ""
    echo " Service => $line"
    echo ""

    container_id=$(docker -H ${__pdci_docker_host}  ps | grep $line | awk -F " " ' { print $1 }')
    container_image_id=""
    container_image=""
    container_error=""
    container_logs=""
    if [ -n ${container_id} ]; then

    	container_image=$(docker -H ${__pdci_docker_host}  ps -a | grep $line | awk -F " " ' { print $2 }')

        if [ "${container_id}" == "" ]; then
            echo ""
            echo "Lendo log do container"
        	container_log_id=$(docker -H ${__pdci_docker_host}  ps -a | grep $line | awk -F " " ' { print $1 }')
        	container_logs=$(docker -H ${__pdci_docker_host}  logs ${container_log_id})
            container_error=$(docker -H ${__pdci_docker_host}  container inspect  --format='{{.State.Error}}' ${container_log_id})
			__nome_file_inspect=${container_log_id}_error_container_inspect
            docker -H ${__pdci_docker_host}  container inspect  ${container_log_id} > ${__nome_file_inspect}
        fi


#        echo "Setando limit de cpf e memoria"
#
#        docker -H ${__pdci_docker_host} update --memory 3072m --memory-swap 3072m --cpus="3"  ${container_id}
#
#        echo "     container_id       => ${container_id}"
#        echo "     container_image_id => ${container_image_id}"
#        echo "     container_image    => ${container_image}"
#        echo "     container_error    => ${container_error}"
#        echo "     container_logs     => ${container_logs}"

    else
      echo "ERRO :: O conteiner do servico $line nao subiu. Verifique o problema"
      exit 1
    fi

    echo ""
done
}

verificar_status_container

#unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | sed -E 's/(.*)=.*/\1/' | xargs)