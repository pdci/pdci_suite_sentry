#!/bin/sh
# wait-for-postgres.sh

set -e

host="$1"
shift
cmd="$@"

#      SENTRY_SECRET_KEY: '08z49s5!zy7f2kg35b+ey2a6#iz-(4*sqdq=^(4epa(j)pc#^x'
#      SENTRY_POSTGRES_HOST: postgres
#      SENTRY_DB_USER: sentry
#      SENTRY_DB_PASSWORD: sentry
#      SENTRY_REDIS_HOST: redis
until PGPASSWORD=${SENTRY_DB_PASSWORD} psql -h "${SENTRY_POSTGRES_HOST}" -U "${SENTRY_DB_USER}" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"
exec $cmd